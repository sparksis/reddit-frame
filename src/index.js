global.Promise = require('bluebird');

const express = require('express');

const app = express();

app.use(express.static('./public'));
app.use(require('./rest/v1'));

// TODO app listen should not be in the /src directory as it should produce a module
// TODO: need better log message.
app.listen(3000, () => console.log('welcome'));
