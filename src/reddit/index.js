const request = require('request-promise');

function logFailure(error) {
    console.warn("Unable to publish record to elastic", error);
}

async function forEachPost(url, callback) {
    let nextPage;
    let pageUrl = new URL(url);
    pageUrl.searchParams.set('limit', 100)
    do {
        pageUrl.searchParams.set('after', nextPage);
        const response = await request(pageUrl.href, { json: true });
        if (response.kind != 'Listing') {
            // TODO throw something here.
        }

        let post;
        while (post = response.data.children.pop()) {
            await callback(post.data);
            console.log('.')
        }
        nextPage = response.data.after;
    } while (nextPage != null);
}

module.exports = {
    forEachPost
}
