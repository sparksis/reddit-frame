const client = require('./client');

const BLACKLISTED_FIELDS = [
    'all_awardings',
    'created',
]; // TODO Config/Constants file.

async function push(post) {
    BLACKLISTED_FIELDS.forEach(field => {
        Reflect.set(post, field, undefined);
    })
    if (!post.edited) {
        // reddit stores this as a timestamp when truethy but boolean when false.
        // To keep data type consistent we'll remove when false.
        post.edited = undefined;
    }
    post.created_utc = new Date(post.created_utc * 1000);
    return await client.index({
        id: post.id,
        index: 'reddit-posts', // TODO: config
        body: post
    });
}

module.exports = push;
