const moment = require('moment');

function createNsfwQuery(nsfw) {
    if (nsfw === undefined) {
        return undefined;
    }
    return {
        "match_phrase": {
            "over_18": {
                "query": nsfw
            }
        }
    }
}
function createLikesQuery(likes) {
    if (likes === undefined) {
        return undefined;
    }
    return {
        "match_phrase": {
            "likes": {
                "query": likes
            }
        }
    }
}
function createSavedQuery(saved) {
    if (saved === undefined) {
        return undefined;
    }
    return {
        "match_phrase": {
            "saved": {
                "query": saved
            }
        }
    }
}
function createSubredditQuery(subreddits) {
    if (!subreddits || subreddits.length == 0) {
        return undefined;
    }
    return {
        "bool": {
            "should": subreddits.map(subreddit => ({
                "match_phrase": {
                    "subreddit": subreddit,
                }
            })),
            "minimum_should_match": 1,
        }
    }
}

function createTimeRangeQuery(range) {
    // TODO: implement
    return {
        "range": {
            "created_utc": {
                "format": "strict_date_optional_time",
                "gte": moment().subtract(30, 'days').toISOString(),
                "lte": moment().toISOString()
            }
        }
    }
}

/**
 * Produce an elasticsearch query from a normalized filter.
 */
function filterToQuery(filter = {}) {
    const queries = [
        // TODO create a 'createBooleanQuery' function and don't repeat yourself
        createNsfwQuery(filter.nsfw),
        createSubredditQuery(filter.subreddits),
        createLikesQuery(filter.likes),
        createSavedQuery(filter.saved),
    ].filter(f => !!f);

    // Push mandatory queries after filtering out undefined queries.
    queries.push(createTimeRangeQuery(undefined));

    return {
        "bool": {
            "must": queries
        }
    };
}

module.exports = filterToQuery;
