const client = require('./client');
const queryBuilder = require('./query-builder');

async function findPosts(filters) {
    const response = await client.search({
        index: 'reddit-posts', // TODO: config
        body: {
            "size": 15, // TODO config/parameters
            "_source": [
                'id',
                'likes',
                'media_embed',
                'media',
                'secure_media',
                'permalink',
                'saved',
                'subreddit',
                'thumbnail',
                'title',
                'ups',
                'url',
            ],
            "sort": [
                {
                    "created_utc": {
                        "order": "desc",
                        "unmapped_type": "boolean"
                    }
                }
            ],
            query: queryBuilder(filters),
        },
    });
    return response.body.hits.hits.map(hit => hit._source);
}

module.exports = findPosts;
