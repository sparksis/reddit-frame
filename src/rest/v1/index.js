const express = require('express');

const app = express();

/**
 * Placeholder content loader.
 */
app.post('/posts', function (req, res) {
    // TODO: define a spec and build it.
    const redditUrl = req.query.fromUrl;
    require('../../reddit').forEachPost(redditUrl, async function (post) {
        try {
            await require('../../repository').savePost(post);
        } catch (error) {
            console.log(error);
        } finally {
            res.send();
        }
    });
});
app.get('/posts', require('./get-posts'));

module.exports = app;
