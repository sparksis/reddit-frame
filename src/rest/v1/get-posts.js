const repository = require('../../repository');

/**
 * @param {Request} req
 * @param {Response} resp
 */
async function doGetPosts(req, res) {
    const filter = req.query;
    for (let prop in filter) {
        let value = Reflect.get(filter, prop);
        if (value == 'all') {
            Reflect.set(filter, prop, undefined);
        }
    }
    try {
        let posts = await repository.findPosts(filter);
        res.send(posts);
    } catch (error) {
        //TODO: logging
        //TODO: error handling
        res.send(error);
    }
}

module.exports = doGetPosts;
