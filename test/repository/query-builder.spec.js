const queryBuilder = require('../../src/repository/query-builder');

const _ = require('lodash');
const assert = require('assert');

describe('queryBuilder', function () {
    it('with an empty filter should have a range of created_utc', function () {
        const query = queryBuilder(undefined);
        const range = _.filter(query.bool.must, { range: { created_utc: {} } }).map(filter => filter.range);
        assert.ok(range);
        assert.equal(range.length, 1);
        assert.equal(range[0].created_utc.format, "strict_date_optional_time");
    });

    describe('with a boolean filter', function () {
        it('{nsfw:true} should only permit nsfw posts', function () {
            const query = queryBuilder({ nsfw: true });
            const over18Query = _(query.bool.must)
                .filter({ match_phrase: { over_18: {} } })
                .head();
            assert.ok(over18Query);
            assert.strictEqual(over18Query.match_phrase.over_18.query, true);
        });

        it('{saved:true} should only permit saved posts', function () {
            const query = queryBuilder({ saved: true });
            const saved = _(query.bool.must)
                .filter({ match_phrase: { saved: {} } })
                .head();
            assert.ok(saved);
            assert.strictEqual(saved.match_phrase.saved.query, true);
        });

        it('{likes:true} should only permit liked posts', function () {
            const query = queryBuilder({ likes: true });
            const likes = _(query.bool.must)
                .filter({ match_phrase: { likes: {} } })
                .head();
            assert.ok(likes);
            assert.strictEqual(likes.match_phrase.likes.query, true)
        });

        it('{nsfw:false} should exclude nsfw posts', function () {
            const query = queryBuilder({ nsfw: false });
            const over18Query = _(query.bool.must)
                .filter({ match_phrase: { over_18: {} } })
                .head();
            assert.ok(over18Query);
            assert.strictEqual(over18Query.match_phrase.over_18.query, false);
        });

        it('{saved:false} should exclude saved posts', function () {
            const query = queryBuilder({ saved: false });
            const saved = _(query.bool.must)
                .filter({ match_phrase: { saved: {} } })
                .head();
            assert.ok(saved);
            assert.strictEqual(saved.match_phrase.saved.query, false);
        });

        it('{likes:false} should exclude liked posts', function () {
            const query = queryBuilder({ likes: false });
            const likes = _(query.bool.must)
                .filter({ match_phrase: { likes: {} } })
                .head();
            assert.ok(likes);
            assert.strictEqual(likes.match_phrase.likes.query, false);
        });

        it('{nsfw:true, likes:true, saved: undefined} should include liked & over_18 posts regardless of saved status', function () {
            const query = queryBuilder({ nsfw: true, likes: true, saved: undefined });
            const likes = _(query.bool.must)
                .filter({ match_phrase: { likes: {} } })
                .head();
            assert.ok(likes);
            assert.strictEqual(likes.match_phrase.likes.query, true);
            const over18Query = _(query.bool.must)
                .filter({ match_phrase: { over_18: {} } })
                .head();
            assert.ok(over18Query);
            assert.strictEqual(over18Query.match_phrase.over_18.query, true);
            const saved = _(query.bool.must)
                .filter({ match_phrase: { saved: {} } })
                .head();
            assert.equal(saved, undefined);
        });
    });

    describe('with a matching filter', function () {
        it('single subreddit should return a single result', function () {
            // TODO: implement
        });
    })
});
