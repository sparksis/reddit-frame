module.exports = {
    elasticsearch: {
        client: {
            node: process.env.ELASTICSEARCH_URL,
            auth: {
                username: process.env.ELASTICSEARCH_USERNAME,
                password: process.env.ELASTICSEARCH_PASSWORD,
            }
        }
    }
}
