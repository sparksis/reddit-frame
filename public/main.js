(function () {
    const $ = jQuery;

    async function search() {
        const filter = $('form#filters').serialize();
        console.log(filter);
        const posts = await $.getJSON('/posts', filter);
        $('div#reddit-posts-place-holder').replaceWith(() => createCards(posts));
    }

    function createCards(posts) {
        const innerHtml = posts.map((post) => `
            <a href="//reddit.com${post.permalink}" class="embedly-card" data-card-controls='0' data-card-width="700px" ></a>
        `).join('');
        return `
        <div id="reddit-posts-place-holder">
            ${innerHtml}
        </div>
        `;
    }

    $(document).ready(search);
    window.search = search;
})();
